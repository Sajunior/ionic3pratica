import { CursosProvider } from './../../providers/cursos/cursos';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DetalhePage } from '../detalhe/detalhe';
import { ICurso } from '../../Interfaces/ICurso';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  lista: ICurso[];

  constructor(
    public navCtrl: NavController, 
    public cursosProvider: CursosProvider
    ){
      //this.lista = this.cursosProvider.all();
  }

  ionViewCanEnter(){
    this.cursosProvider.allTeste().subscribe(res => {
      this.lista = res;
    }, erro => {
      console.log("Erro:" + erro.message);
    })

    let data:ICurso = {

        "titulo": "Curso de Ionic 77777", 
        "descricao": "Aprenda Ionic na Pratica", 
        "valor": 23.90, 
        "valor_txt": "23,90", 
        "imagem": "https://miro.medium.com/max/2400/1*7m6SuqOuypmFZy6OuiHP8w.jpeg",
        "aulas":[
          {
            "id":1,
            "ordem":1,
            "titulo":"Introducao ao Curso",
            "tempo":"10:00",
            "video":"https://www.youtube.com/embed/RUMg5h3xZ-s"
          },
          {
            "id":2,
            "ordem":2,
            "titulo":"Realaizando a Instalacão",
            "tempo":"05:00",
            "video":"https://www.youtube.com/embed/Yc4F2tWEtG0"
          }
        ]
      };
    
    this.cursosProvider.addTeste(data).subscribe(res => {
    }, erro => {
      console.log("Erro:" + erro.message);
    });

  }

  abreDetalhe(item){
    this.navCtrl.push(DetalhePage,{dados:item});
  }

}
