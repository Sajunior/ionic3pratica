import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ICurso } from '../../Interfaces/ICurso';


/*
  Generated class for the CursosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CursosProvider {

  constructor(public http: HttpClient) {
    console.log('Hello CursosProvider Provider');
  }

  allTeste(){
    return this.http.get<ICurso[]>('http://localhost:3000/cursos')
  }

  addTeste(data:ICurso){
    return this.http.post<ICurso>('http://localhost:3000/cursos',data)
  }

  all(){
   const lista: ICurso[] = [
      { 
        "id": 1, 
        "titulo": "Curso de Ionic", 
        "descricao": "Aprenda Ionic na Pratica", 
        "valor": 23.90, 
        "valor_txt": "23,90", 
        "imagem": "https://miro.medium.com/max/2400/1*7m6SuqOuypmFZy6OuiHP8w.jpeg",
        "aulas":[
          {
            "id":1,
            "ordem":1,
            "titulo":"Introducao ao Curso",
            "tempo":"10:00",
            "video":"https://www.youtube.com/embed/RUMg5h3xZ-s"
          },
          {
            "id":2,
            "ordem":2,
            "titulo":"Realaizando a Instalacão",
            "tempo":"05:00",
            "video":"https://www.youtube.com/embed/Yc4F2tWEtG0"
          }
        ]
      },
      { "id": 2, 
        "titulo": "Curso de Js", 
        "descricao": "Aprenda Js na Pratica", 
        "valor": 33.90, 
        "valor_txt": "33,90", 
        "imagem": "https://miro.medium.com/max/2400/1*7m6SuqOuypmFZy6OuiHP8w.jpeg",
        "aulas":[]
      }
    ];

    return lista;

  }


}
